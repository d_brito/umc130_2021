* SPICE NETLIST
***************************************

.SUBCKT N_12_RF D G S B
.ENDS
***************************************
.SUBCKT P_12_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_33_RF D G S B
.ENDS
***************************************
.SUBCKT P_33_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_BPW_12_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT N_BPW_33_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT VARMIS_12_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARMIS_33_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARDIOP_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT RNNPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNPPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNHR_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MIMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT PAD_RF PLUS PSUB
.ENDS
***************************************
.SUBCKT DIOP_ESD_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT DIODN_ESD_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT NPN_SV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_SVL20_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NVL20_RF C B E S
.ENDS
***************************************
.SUBCKT PNP_NV50X50_RF C B E
.ENDS
***************************************
.SUBCKT PNP_NVL20_RF C B E
.ENDS
***************************************
.SUBCKT L_CR20K_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWCR20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_CR20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20KCT_RFVIL PLUS MINUS CT NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20KCT_RFVIL PLUS MINUS CT PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWSQSK_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SYMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP3_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP4_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_615291889232 1 2 3
** N=3 EP=3 IP=0 FDC=1
M0 3 2 3 1 N_12_HSL130E L=9.62812e-07 W=2.68456e-05 $X=-3075 $Y=-5525 $D=3
.ENDS
***************************************
.SUBCKT inverter VIN VOUT VSS VDD
** N=4 EP=4 IP=0 FDC=2
M0 VOUT VIN VSS VSS N_12_HSL130E L=3.01568e-07 W=5.96399e-06 $X=4625 $Y=-21095 $D=3
M1 VOUT VIN VDD VDD P_12_HSL130E L=3.00547e-07 W=2.71706e-05 $X=2750 $Y=-12260 $D=4
.ENDS
***************************************
.SUBCKT RO VSS VDD Vout Vctrl
** N=6 EP=4 IP=21 FDC=9
X0 VSS Vctrl 3 N_12_HSL130E_CDNS_615291889232 $T=9995 -31695 1 0 $X=5620 $Y=-34300
X1 VSS Vctrl 5 N_12_HSL130E_CDNS_615291889232 $T=21155 -31695 1 0 $X=16780 $Y=-34300
X2 VSS Vctrl Vout N_12_HSL130E_CDNS_615291889232 $T=32315 -31695 1 0 $X=27940 $Y=-34300
X3 Vout 3 VSS VDD inverter $T=4925 350 0 0 $X=6130 $Y=-22780
X4 3 5 VSS VDD inverter $T=15725 350 0 0 $X=16930 $Y=-22780
X5 5 Vout VSS VDD inverter $T=26525 350 0 0 $X=27730 $Y=-22780
.ENDS
***************************************
