* SPICE NETLIST
***************************************

.SUBCKT N_12_RF D G S B
.ENDS
***************************************
.SUBCKT P_12_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_33_RF D G S B
.ENDS
***************************************
.SUBCKT P_33_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_BPW_12_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT N_BPW_33_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT VARMIS_12_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARMIS_33_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARDIOP_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT RNNPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNPPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNHR_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MIMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT PAD_RF PLUS PSUB
.ENDS
***************************************
.SUBCKT DIOP_ESD_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT DIODN_ESD_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT NPN_SV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_SVL20_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NVL20_RF C B E S
.ENDS
***************************************
.SUBCKT PNP_NV50X50_RF C B E
.ENDS
***************************************
.SUBCKT PNP_NVL20_RF C B E
.ENDS
***************************************
.SUBCKT L_CR20K_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWCR20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_CR20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20KCT_RFVIL PLUS MINUS CT NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20KCT_RFVIL PLUS MINUS CT PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWSQSK_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SYMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP3_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP4_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_615303114020 1 2 3
** N=3 EP=3 IP=0 FDC=1
M0 3 2 1 1 N_12_HSL130E L=3.01568e-07 W=5.96399e-06 $X=-740 $Y=-1190 $D=3
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3
** N=3 EP=3 IP=6 FDC=2
X0 1 2 3 N_12_HSL130E_CDNS_615303114020 $T=0 0 0 0 $X=-2150 $Y=-3225
X1 1 2 3 N_12_HSL130E_CDNS_615303114020 $T=4320 0 0 0 $X=2170 $Y=-3225
.ENDS
***************************************
.SUBCKT P_12_HSL130E_CDNS_615303114021 1 2 3
** N=4 EP=3 IP=0 FDC=1
M0 3 2 1 1 P_12_HSL130E L=3.00547e-07 W=2.71706e-05 $X=-2615 $Y=-5715 $D=4
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5
** N=6 EP=5 IP=8 FDC=2
X0 1 2 4 P_12_HSL130E_CDNS_615303114021 $T=0 0 0 0 $X=-4160 $Y=-9760
X1 1 3 5 P_12_HSL130E_CDNS_615303114021 $T=10800 0 0 0 $X=6640 $Y=-9760
.ENDS
***************************************
.SUBCKT buffer VSS VDD Vin Vout
** N=7 EP=4 IP=72 FDC=30
X0 VSS Vin 4 N_12_HSL130E_CDNS_615303114020 $T=17340 -52420 0 0 $X=15190 $Y=-55645
X1 VSS 4 5 ICV_1 $T=32710 -52420 0 0 $X=30560 $Y=-55645
X2 VSS 5 6 ICV_1 $T=48910 -57100 0 0 $X=46760 $Y=-60325
X3 VSS 5 6 ICV_1 $T=48910 -52420 0 0 $X=46760 $Y=-55645
X4 VSS 6 Vout ICV_1 $T=60790 -57100 0 0 $X=58640 $Y=-60325
X5 VSS 6 Vout ICV_1 $T=60790 -52420 0 0 $X=58640 $Y=-55645
X6 VSS 6 Vout ICV_1 $T=69430 -57100 0 0 $X=67280 $Y=-60325
X7 VSS 6 Vout ICV_1 $T=69430 -52420 0 0 $X=67280 $Y=-55645
X8 VDD 6 Vout P_12_HSL130E_CDNS_615303114021 $T=71340 -36005 1 0 $X=67180 $Y=-38205
X9 VDD 6 Vout P_12_HSL130E_CDNS_615303114021 $T=71340 -24845 1 0 $X=67180 $Y=-27045
X10 VDD 6 Vout P_12_HSL130E_CDNS_615303114021 $T=71340 -13685 1 0 $X=67180 $Y=-15885
X11 VDD Vin 4 4 5 ICV_2 $T=17340 -47165 1 0 $X=13180 $Y=-49365
X12 VDD 4 5 5 6 ICV_2 $T=38940 -47165 1 0 $X=34780 $Y=-49365
X13 VDD 5 6 6 Vout ICV_2 $T=49740 -36005 1 0 $X=45580 $Y=-38205
X14 VDD 5 6 6 Vout ICV_2 $T=49740 -24845 1 0 $X=45580 $Y=-27045
X15 VDD 5 6 6 Vout ICV_2 $T=49740 -13685 1 0 $X=45580 $Y=-15885
X16 VDD 6 6 Vout Vout ICV_2 $T=60540 -47165 1 0 $X=56380 $Y=-49365
.ENDS
***************************************
