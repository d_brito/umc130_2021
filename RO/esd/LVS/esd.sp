* SPICE NETLIST
***************************************

.SUBCKT N_12_RF D G S B
.ENDS
***************************************
.SUBCKT P_12_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_33_RF D G S B
.ENDS
***************************************
.SUBCKT P_33_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_BPW_12_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT N_BPW_33_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT VARMIS_12_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARMIS_33_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARDIOP_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT RNNPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNPPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNHR_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MIMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT PAD_RF PLUS PSUB
.ENDS
***************************************
.SUBCKT DIOP_ESD_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT DIODN_ESD_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT NPN_SV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_SVL20_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NVL20_RF C B E S
.ENDS
***************************************
.SUBCKT PNP_NV50X50_RF C B E
.ENDS
***************************************
.SUBCKT PNP_NVL20_RF C B E
.ENDS
***************************************
.SUBCKT L_CR20K_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWCR20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_CR20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20KCT_RFVIL PLUS MINUS CT NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20KCT_RFVIL PLUS MINUS CT PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWSQSK_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SYMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP3_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP4_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT P_HG_33_L130E_CDNS_615462631181 1 2
** N=3 EP=2 IP=0 FDC=10
M0 2 1 1 1 P_HG_33_L130E L=4.4e-07 W=3e-06 $X=0 $Y=180 $D=10
M1 1 1 2 1 P_HG_33_L130E L=4.4e-07 W=3e-06 $X=880 $Y=180 $D=10
M2 2 1 1 1 P_HG_33_L130E L=4.4e-07 W=3e-06 $X=1760 $Y=180 $D=10
M3 1 1 2 1 P_HG_33_L130E L=4.4e-07 W=3e-06 $X=2640 $Y=180 $D=10
M4 2 1 1 1 P_HG_33_L130E L=4.4e-07 W=3e-06 $X=3520 $Y=180 $D=10
M5 1 1 2 1 P_HG_33_L130E L=4.4e-07 W=3e-06 $X=4400 $Y=180 $D=10
M6 2 1 1 1 P_HG_33_L130E L=4.4e-07 W=3e-06 $X=5280 $Y=180 $D=10
M7 1 1 2 1 P_HG_33_L130E L=4.4e-07 W=3e-06 $X=6160 $Y=180 $D=10
M8 2 1 1 1 P_HG_33_L130E L=4.4e-07 W=3e-06 $X=7040 $Y=180 $D=10
M9 1 1 2 1 P_HG_33_L130E L=4.4e-07 W=3e-06 $X=7920 $Y=180 $D=10
.ENDS
***************************************
.SUBCKT N_HG_33_L130E_CDNS_615462631180 1 2
** N=2 EP=2 IP=0 FDC=10
M0 2 1 1 1 N_HG_33_L130E L=4.4e-07 W=3e-06 $X=0 $Y=180 $D=9
M1 1 1 2 1 N_HG_33_L130E L=4.4e-07 W=3e-06 $X=880 $Y=180 $D=9
M2 2 1 1 1 N_HG_33_L130E L=4.4e-07 W=3e-06 $X=1760 $Y=180 $D=9
M3 1 1 2 1 N_HG_33_L130E L=4.4e-07 W=3e-06 $X=2640 $Y=180 $D=9
M4 2 1 1 1 N_HG_33_L130E L=4.4e-07 W=3e-06 $X=3520 $Y=180 $D=9
M5 1 1 2 1 N_HG_33_L130E L=4.4e-07 W=3e-06 $X=4400 $Y=180 $D=9
M6 2 1 1 1 N_HG_33_L130E L=4.4e-07 W=3e-06 $X=5280 $Y=180 $D=9
M7 1 1 2 1 N_HG_33_L130E L=4.4e-07 W=3e-06 $X=6160 $Y=180 $D=9
M8 2 1 1 1 N_HG_33_L130E L=4.4e-07 W=3e-06 $X=7040 $Y=180 $D=9
M9 1 1 2 1 N_HG_33_L130E L=4.4e-07 W=3e-06 $X=7920 $Y=180 $D=9
.ENDS
***************************************
.SUBCKT M1_POLY_CDNS_615462631181
** N=2 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT esd PAD GND VDD
** N=3 EP=3 IP=24 FDC=80
X0 VDD PAD P_HG_33_L130E_CDNS_615462631181 $T=4390 -10745 0 0 $X=3530 $Y=-11065
X1 VDD PAD P_HG_33_L130E_CDNS_615462631181 $T=4390 -6945 0 0 $X=3530 $Y=-7265
X2 VDD PAD P_HG_33_L130E_CDNS_615462631181 $T=14070 -10745 0 0 $X=13210 $Y=-11065
X3 VDD PAD P_HG_33_L130E_CDNS_615462631181 $T=14070 -6945 0 0 $X=13210 $Y=-7265
X4 GND PAD N_HG_33_L130E_CDNS_615462631180 $T=4390 -18795 0 0 $X=3730 $Y=-19015
X5 GND PAD N_HG_33_L130E_CDNS_615462631180 $T=4390 -14995 0 0 $X=3730 $Y=-15215
X6 GND PAD N_HG_33_L130E_CDNS_615462631180 $T=14070 -18795 0 0 $X=13410 $Y=-19015
X7 GND PAD N_HG_33_L130E_CDNS_615462631180 $T=14070 -14995 0 0 $X=13410 $Y=-15215
.ENDS
***************************************
