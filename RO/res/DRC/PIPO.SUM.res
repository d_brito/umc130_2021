********************************************************************************
Product   : Virtuoso(R) XStream Out
Program   : @(#)$CDS: strmout version 6.1.8-64b 08/27/2019 20:18 (cpgbld02.cadence.com) $
          : sub-version  IC6.1.8-64b.500.6 
Started at: 15-Mar-2021  10:50:10
User Name : gtrindade
Host Name : MSC5
Directory : /home/gtrindade/Cadence/Projects/umc130_ELT
CADENCE Design Systems, Inc.
********************************************************************************
Info: Cellview Rev Num:99,  Tech Rev Num:59


                       Individual Cell Statistics - Basic Objects
---------------------------------------------------------------------------------------------------------------------------------------
Library/Cell/View               Instance Array   Polygon  Rect   Path   PathSeg   Text   TextDisplay   Line   Dot   Arc   Donut   Ellipse 
---------------------------------------------------------------------------------------------------------------------------------------
RO/res/layout                   2        0       0        4      0      1         3      0             0      0     0     0       0       
umc13mmrf/M1_NWEL/layout        0        0       0        20     0      0         0      0             0      0     0     0       0       
umc13mmrf/RNHR1000_MML130E/layout                               0        0       0        11     0      0         5      0             0      0     0     0       0       
---------------------------------------------------------------------------------------------------------------------------------------

                       Individual Cell Statistics - Advanced Objects
---------------------------------------------------------------------------------------------------------------------------------------
Library/Cell/View               PRBdy    OtherBdy   AreaBlkg   LayerBlkg  AreaHalo   Row   Marker   CustVia   StdVia    CdsGenVia
---------------------------------------------------------------------------------------------------------------------------------------
RO/res/layout                   0        0          0          0          0          0     0        0         1         0        
umc13mmrf/M1_NWEL/layout        0        0          0          0          0          0     0        0         0         0        
umc13mmrf/RNHR1000_MML130E/layout                               0        0          0          0          0          0     0        0         0         0        
---------------------------------------------------------------------------------------------------------------------------------------

                       Statistics of Layers 
---------------------------------------------------------------------------------------------------------------------------------------
Cadence         Cadence         Stream  Stream   
Layer           Purpose         Layer   Datatype  Polygon  Rect     Path     Text     TextDisplay   Line Dot Arc Donut Ellipse Pathseg
---------------------------------------------------------------------------------------------------------------------------------------
IPWM            drawing         63      63        0        0        0        5        0             0    0   0   0     0      0      
NWEL            drawing         3       0         0        3        0        0        0             0    0   0   0     0      0      
M1_CAD          TEXT            101     0         0        0        0        3        0             0    0   0   0     0      0      
PSYMBOL         drawing         82      0         0        1        0        0        0             0    0   0   0     0      0      
PPLUS           drawing         11      0         0        2        0        0        0             0    0   0   0     0      0      
NPLUS           drawing         12      0         0        1        0        0        0             0    0   0   0     0      0      
PO1             drawing         41      0         0        1        0        0        0             0    0   0   0     0      0      
ME1             pin             46      0         0        3        0        0        0             0    0   0   0     0      0      
CONT            drawing         39      0         0        18       0        0        0             0    0   0   0     0      0      
SAB             drawing         36      0         0        1        0        0        0             0    0   0   0     0      0      
HR              drawing         38      0         0        1        0        0        0             0    0   0   0     0      0      
ME1             drawing         46      0         0        3        0        0        0             0    0   0   0     0      1      
DIFF            drawing         1       0         0        1        0        0        0             0    0   0   0     0      0      
---------------------------------------------------------------------------------------------------------------------------------------

Summary of Objects Translated:
	Scalar Instances:		2
	Array Instances:		0
	Polygons:			0
	Paths:				0
	Rectangles:			35
	Lines:				0
	Arcs:				0
	Donuts:				0
	Dots:				0
	Ellipses:			0
	Boundaries:			0
	Area Blockages:			0
	Layer Blockages:		0
	Area Halos:			0
	Markers:			0
	Rows:				0
	Standard Vias:			1
	Custom Vias:			0
	CdsGen Vias:			0
	Pathsegs:			1
	Text:				8
	TextDisplay:		0
	Cells:				3
Design Libraries: 

DEFINE RO	/home/gtrindade/Cadence/Projects/umc130_ELT/RO
DEFINE umc13mmrf	/opt/pdk/umc13mmrf/umc13mmrf
