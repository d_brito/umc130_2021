********************************************************************************
Product   : Virtuoso(R) XStream Out
Program   : @(#)$CDS: strmout version 6.1.8-64b 08/27/2019 20:18 (cpgbld02.cadence.com) $
          : sub-version  IC6.1.8-64b.500.6 
Started at: 15-Mar-2021  11:04:45
User Name : gtrindade
Host Name : MSC5
Directory : /home/gtrindade/Cadence/Projects/umc130_ELT
CADENCE Design Systems, Inc.
********************************************************************************
Info: Cellview Rev Num:99,  Tech Rev Num:59

WARNING (XSTRM-20): Output Stream file '/home/gtrindade/Cadence/Projects/umc130_ELT/RO/res/PEX/res.calibre.db' already exists. It will be overwritten.
INFO (XSTRM-217): Reading the layer map file, /opt/pdk/umc13mmrf/umc13mmrf/umc13mmrf.layermap
INFO (XSTRM-162): You have not used the viaMap option. If the OpenAccess design has native oaVia instances, use the -viaMap option for preserving oaVia instances in a Stream Out - Stream In round trip. Using the -viaMap option improves performance and VM usage of applications using the Streamed-In design.  For details on the viaMap option, refer to the "Design Data Translator's Reference" guide for XStream.

Summary of Options :
library                                 RO
strmFile                                /home/gtrindade/Cadence/Projects/umc130_ELT/RO/res/PEX/res.calibre.db
topCell                                 res
view                                    layout
runDir                                  /home/gtrindade/Cadence/Projects/umc130_ELT/RO/res/PEX/
logFile                                 PIPO.LOG.res
summaryFile                             PIPO.SUM.res
layerMap                                /opt/pdk/umc13mmrf/umc13mmrf/umc13mmrf.layermap
case                                    Preserve
convertDot                              node
convertPcellPin                         geometry

INFO (XSTRM-223): 1. Translating stdVia umc13mmrf/M1_NWEL/layout as STRUCTURE M1_NWEL_CDNS_615806285160.
INFO (XSTRM-223): 2. Translating cellView umc13mmrf/RNHR1000_MML130E/layout as STRUCTURE RNHR1000_MML130E_CDNS_615806285160.
INFO (XSTRM-223): 3. Translating cellView RO/res/layout as STRUCTURE res.

Summary of Objects Translated:
	Scalar Instances:                       2
	Array Instances:                        0
	Polygons:                               0
	Paths:                                  0
	Rectangles:                             35
	Lines:                                  0
	Arcs:                                   0
	Donuts:                                 0
	Dots:                                   0
	Ellipses:                               0
	Boundaries:                             0
	Area Blockages:                         0
	Layer Blockages:                        0
	Area Halos:                             0
	Markers:                                0
	Rows:                                   0
	Standard Vias                           1
	Custom Vias:                            0
	CdsGen Vias:                            0
	Pathsegs:                               1
	Text:                                   8
	TextDisplay:                            0
	Cells:                                  3

Elapsed Time: 1.8s   User Time: 0.6s   CPU Time: 0.1s   Peak VM: 24620KB
INFO (XSTRM-234): Translation completed. '0' error(s) and '1' warning(s) found.
