* SPICE NETLIST
***************************************

.SUBCKT N_12_RF D G S B
.ENDS
***************************************
.SUBCKT P_12_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_33_RF D G S B
.ENDS
***************************************
.SUBCKT P_33_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_BPW_12_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT N_BPW_33_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT VARMIS_12_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARMIS_33_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARDIOP_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT RNNPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNPPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNHR_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MIMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT PAD_RF PLUS PSUB
.ENDS
***************************************
.SUBCKT DIOP_ESD_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT DIODN_ESD_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT NPN_SV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_SVL20_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NVL20_RF C B E S
.ENDS
***************************************
.SUBCKT PNP_NV50X50_RF C B E
.ENDS
***************************************
.SUBCKT PNP_NVL20_RF C B E
.ENDS
***************************************
.SUBCKT L_CR20K_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWCR20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_CR20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20KCT_RFVIL PLUS MINUS CT NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20KCT_RFVIL PLUS MINUS CT PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWSQSK_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SYMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP3_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP4_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT buffer_jrf Input Output GND supplyVDD
** N=8 EP=4 IP=0 FDC=30
M0 2 Input GND GND N_12_HSL130E L=1.2e-07 W=3.2e-07 $X=3840 $Y=3740 $D=3
M1 3 2 GND GND N_12_HSL130E L=1.2e-07 W=4.8e-07 $X=4960 $Y=3580 $D=3
M2 4 3 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=6080 $Y=2620 $D=3
M3 GND 4 5 GND N_12_HSL130E L=1.2e-07 W=1.15e-06 $X=7160 $Y=2910 $D=3
M4 5 4 GND GND N_12_HSL130E L=1.2e-07 W=1.15e-06 $X=7680 $Y=2910 $D=3
M5 GND 4 5 GND N_12_HSL130E L=1.2e-07 W=1.15e-06 $X=8200 $Y=2910 $D=3
M6 Output 5 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=8760 $Y=2620 $D=3
M7 GND 5 Output GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=9280 $Y=2620 $D=3
M8 Output 5 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=9800 $Y=2620 $D=3
M9 GND 5 Output GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=10320 $Y=2620 $D=3
M10 Output 5 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=10840 $Y=2620 $D=3
M11 GND 5 Output GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=11360 $Y=2620 $D=3
M12 Output 5 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=11880 $Y=2620 $D=3
M13 GND 5 Output GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=12400 $Y=2620 $D=3
M14 Output 5 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=12920 $Y=2620 $D=3
M15 2 Input supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=9.6e-07 $X=3840 $Y=5060 $D=4
M16 3 2 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=1.44e-06 $X=4960 $Y=5060 $D=4
M17 4 3 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=3.455e-06 $X=6080 $Y=5060 $D=4
M18 supplyVDD 4 5 supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=7200 $Y=5060 $D=4
M19 5 4 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=7720 $Y=5060 $D=4
M20 supplyVDD 4 5 supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=8240 $Y=5060 $D=4
M21 Output 5 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=8760 $Y=5060 $D=4
M22 supplyVDD 5 Output supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=9280 $Y=5060 $D=4
M23 Output 5 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=9800 $Y=5060 $D=4
M24 supplyVDD 5 Output supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=10320 $Y=5060 $D=4
M25 Output 5 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=10840 $Y=5060 $D=4
M26 supplyVDD 5 Output supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=11360 $Y=5060 $D=4
M27 Output 5 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=11880 $Y=5060 $D=4
M28 supplyVDD 5 Output supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=12400 $Y=5060 $D=4
M29 Output 5 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=12920 $Y=5060 $D=4
.ENDS
***************************************
