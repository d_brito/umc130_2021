* SPICE NETLIST
***************************************

.SUBCKT N_12_RF D G S B
.ENDS
***************************************
.SUBCKT P_12_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_33_RF D G S B
.ENDS
***************************************
.SUBCKT P_33_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_BPW_12_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT N_BPW_33_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT VARMIS_12_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARMIS_33_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARDIOP_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT RNNPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNPPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNHR_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MIMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT PAD_RF PLUS PSUB
.ENDS
***************************************
.SUBCKT DIOP_ESD_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT DIODN_ESD_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT NPN_SV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_SVL20_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NVL20_RF C B E S
.ENDS
***************************************
.SUBCKT PNP_NV50X50_RF C B E
.ENDS
***************************************
.SUBCKT PNP_NVL20_RF C B E
.ENDS
***************************************
.SUBCKT L_CR20K_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWCR20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_CR20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20KCT_RFVIL PLUS MINUS CT NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20KCT_RFVIL PLUS MINUS CT PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWSQSK_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SYMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP3_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP4_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_615974805065
** N=4 EP=0 IP=0 FDC=0
*.SEEDPROM
.ENDS
***************************************
.SUBCKT divideby2 in out rst_n vss vdd
** N=10 EP=5 IP=8 FDC=10
M0 10 in out vss N_12_HSL130E L=1.2e-07 W=3.2e-07 AD=4.16e-14 AS=1.088e-13 PD=5.8e-07 PS=1.32e-06 sa=3.4e-07 sb=1.24e-06 $X=4890 $Y=-5395 $D=3
M1 6 2 10 vss N_12_HSL130E L=1.2e-07 W=3.2e-07 AD=6.4e-14 AS=4.16e-14 PD=7.2e-07 PS=5.8e-07 sa=7.2e-07 sb=8.6e-07 $X=5270 $Y=-5395 $D=3
M2 5 out vss vss N_12_HSL130E L=1.2e-07 W=1.6e-07 AD=9.44e-14 AS=4.16e-14 PD=1.32e-06 PS=5.06667e-07 sa=9e-07 sb=3.8e-07 $X=6350 $Y=-6335 $D=3
M3 vss in 6 vss N_12_HSL130E L=1.2e-07 W=3.2e-07 AD=8.32e-14 AS=1.088e-13 PD=1.01333e-06 PS=1.32e-06 sa=3.4e-07 sb=5.11429e-07 $X=5790 $Y=-6435 $D=3
M4 2 5 6 vss N_12_HSL130E L=1.2e-07 W=3.2e-07 AD=1.088e-13 AS=6.4e-14 PD=1.32e-06 PS=7.2e-07 sa=1.24e-06 sb=3.4e-07 $X=5790 $Y=-5395 $D=3
M5 vdd in 2 vdd P_12_HSL130E L=1.2e-07 W=7.2e-07 AD=1.44e-13 AS=2.448e-13 PD=1.12e-06 PS=2.12e-06 sa=3.4e-07 sb=8.6e-07 $X=5330 $Y=-2710 $D=4
M6 out 2 vdd vdd P_12_HSL130E L=1.2e-07 W=7.2e-07 AD=2.448e-13 AS=1.44e-13 PD=2.12e-06 PS=1.12e-06 sa=8.6e-07 sb=3.4e-07 $X=5850 $Y=-2710 $D=4
M7 9 in 5 vdd P_12_HSL130E L=1.2e-07 W=1.44e-06 AD=1.872e-13 AS=4.896e-13 PD=1.7e-06 PS=3.56e-06 sa=3.4e-07 sb=7.2e-07 $X=7250 $Y=-3430 $D=4
M8 vdd out 9 vdd P_12_HSL130E L=1.2e-07 W=1.44e-06 AD=4.896e-13 AS=1.872e-13 PD=3.56e-06 PS=1.7e-06 sa=7.2e-07 sb=3.4e-07 $X=7630 $Y=-3430 $D=4
M9 out rst_n vdd vdd P_12_HSL130E L=1.2e-07 W=1.6e-07 AD=9.44e-14 AS=9.44e-14 PD=1.32e-06 PS=1.32e-06 sa=3.8e-07 sb=3.8e-07 $X=9070 $Y=-2210 $D=4
.ENDS
***************************************
