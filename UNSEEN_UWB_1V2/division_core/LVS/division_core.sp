* SPICE NETLIST
***************************************

.SUBCKT N_12_RF D G S B
.ENDS
***************************************
.SUBCKT P_12_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_33_RF D G S B
.ENDS
***************************************
.SUBCKT P_33_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_BPW_12_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT N_BPW_33_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT VARMIS_12_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARMIS_33_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARDIOP_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT RNNPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNPPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNHR_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MIMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT PAD_RF PLUS PSUB
.ENDS
***************************************
.SUBCKT DIOP_ESD_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT DIODN_ESD_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT NPN_SV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_SVL20_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NVL20_RF C B E S
.ENDS
***************************************
.SUBCKT PNP_NV50X50_RF C B E
.ENDS
***************************************
.SUBCKT PNP_NVL20_RF C B E
.ENDS
***************************************
.SUBCKT L_CR20K_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWCR20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_CR20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20KCT_RFVIL PLUS MINUS CT NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20KCT_RFVIL PLUS MINUS CT PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWSQSK_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SYMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP3_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP4_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT M1_POLY_CDNS_6160560168419
** N=2 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NCAP_HSL130E_CDNS_6160560168413 1 2
** N=2 EP=2 IP=0 FDC=1
M0 1 2 1 1 N_12_HSL130E L=2.1e-06 W=4e-06 $X=340 $Y=0 $D=3
.ENDS
***************************************
.SUBCKT M1_NWEL_CDNS_6160560168421
** N=2 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT M1_PACTIVE_CDNS_6160560168420
** N=1 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT INV2 in vss vdd out
** N=4 EP=4 IP=0 FDC=2
M0 out in vss vss N_12_HSL130E L=2.4e-07 W=4.8e-07 $X=2795 $Y=-6220 $D=3
M1 out in vdd vdd P_12_HSL130E L=2.4e-07 W=2.16e-06 $X=2795 $Y=-4090 $D=4
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_616056016847 1 2 3 4
** N=4 EP=4 IP=0 FDC=1
M0 2 3 1 4 N_12_HSL130E L=1.2e-07 W=3.2e-07 $X=0 $Y=180 $D=3
.ENDS
***************************************
.SUBCKT divideby2 rst_n in out vss vdd
** N=10 EP=5 IP=8 FDC=10
M0 10 in out vss N_12_HSL130E L=1.2e-07 W=3.2e-07 $X=4890 $Y=-5395 $D=3
M1 6 7 10 vss N_12_HSL130E L=1.2e-07 W=3.2e-07 $X=5270 $Y=-5395 $D=3
M2 8 out vss vss N_12_HSL130E L=1.2e-07 W=1.6e-07 $X=6350 $Y=-6335 $D=3
M3 vdd rst_n out vdd P_12_HSL130E L=1.2e-07 W=1.6e-07 $X=3890 $Y=-2210 $D=4
M4 vdd in 7 vdd P_12_HSL130E L=1.2e-07 W=7.2e-07 $X=5330 $Y=-2710 $D=4
M5 out 7 vdd vdd P_12_HSL130E L=1.2e-07 W=7.2e-07 $X=5850 $Y=-2710 $D=4
M6 9 in 8 vdd P_12_HSL130E L=1.2e-07 W=1.44e-06 $X=7250 $Y=-3430 $D=4
M7 vdd out 9 vdd P_12_HSL130E L=1.2e-07 W=1.44e-06 $X=7630 $Y=-3430 $D=4
X8 vss 6 in vss N_12_HSL130E_CDNS_616056016847 $T=5910 -6615 1 180 $X=5290 $Y=-6675
X9 6 7 8 vss N_12_HSL130E_CDNS_616056016847 $T=5790 -5575 0 0 $X=5290 $Y=-5635
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_6160560168411 1 2 3 4
** N=4 EP=4 IP=0 FDC=1
M0 2 3 1 4 N_12_HSL130E L=1.2e-07 W=5e-07 $X=0 $Y=180 $D=3
.ENDS
***************************************
.SUBCKT P_12_HSL130E_CDNS_6160560168412 1 2 3 4
** N=5 EP=4 IP=0 FDC=1
M0 2 3 1 4 P_12_HSL130E L=1.2e-07 W=1.5e-06 $X=0 $Y=180 $D=4
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_6160560168425 1 2 3 4
** N=4 EP=4 IP=0 FDC=1
M0 2 3 1 4 N_12_HSL130E L=1.2e-07 W=5e-07 $X=0 $Y=180 $D=3
.ENDS
***************************************
.SUBCKT P_12_HSL130E_CDNS_6160560168423 1 2 3 4
** N=5 EP=4 IP=0 FDC=1
M0 2 3 1 4 P_12_HSL130E L=1.2e-07 W=1.5e-06 $X=0 $Y=180 $D=4
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_6160560168424 1 2 3 4
** N=4 EP=4 IP=0 FDC=1
M0 3 2 1 4 N_12_HSL130E L=1.2e-07 W=5e-07 $X=0 $Y=180 $D=3
.ENDS
***************************************
.SUBCKT P_12_HSL130E_CDNS_6160560168426 1 2 3 4
** N=5 EP=4 IP=0 FDC=1
M0 3 2 1 4 P_12_HSL130E L=1.2e-07 W=1.5e-06 $X=0 $Y=180 $D=4
.ENDS
***************************************
.SUBCKT division_core GND final supplyVDD div output div1 input rst_n
** N=25 EP=8 IP=141 FDC=101
M0 3 2 GND GND N_12_HSL130E L=1.2e-07 W=4.8e-07 $X=11445 $Y=-7875 $D=3
M1 5 3 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=12565 $Y=-7875 $D=3
M2 GND 5 6 GND N_12_HSL130E L=1.2e-07 W=1.15e-06 $X=13645 $Y=-7875 $D=3
M3 6 5 GND GND N_12_HSL130E L=1.2e-07 W=1.15e-06 $X=14165 $Y=-7875 $D=3
M4 GND 5 6 GND N_12_HSL130E L=1.2e-07 W=1.15e-06 $X=14685 $Y=-7875 $D=3
M5 final 6 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=15245 $Y=-7875 $D=3
M6 GND 6 final GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=15765 $Y=-7875 $D=3
M7 final 6 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=16285 $Y=-7875 $D=3
M8 GND 6 final GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=16805 $Y=-7875 $D=3
M9 final 6 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=17325 $Y=-7875 $D=3
M10 GND 6 final GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=17845 $Y=-7875 $D=3
M11 final 6 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=18365 $Y=-7875 $D=3
M12 GND 6 final GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=18885 $Y=-7875 $D=3
M13 final 6 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=19405 $Y=-7875 $D=3
M14 2 1 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=9.6e-07 $X=10325 $Y=-9835 $D=4
M15 3 2 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=1.44e-06 $X=11445 $Y=-10315 $D=4
M16 5 3 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=3.455e-06 $X=12565 $Y=-12330 $D=4
M17 supplyVDD 5 6 supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=13685 $Y=-13195 $D=4
M18 6 5 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=14205 $Y=-13195 $D=4
M19 supplyVDD 5 6 supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=14725 $Y=-13195 $D=4
M20 final 6 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=15245 $Y=-13195 $D=4
M21 supplyVDD 6 final supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=15765 $Y=-13195 $D=4
M22 final 6 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=16285 $Y=-13195 $D=4
M23 supplyVDD 6 final supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=16805 $Y=-13195 $D=4
M24 final 6 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=17325 $Y=-13195 $D=4
M25 supplyVDD 6 final supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=17845 $Y=-13195 $D=4
M26 final 6 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=18365 $Y=-13195 $D=4
M27 supplyVDD 6 final supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=18885 $Y=-13195 $D=4
M28 final 6 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 $X=19405 $Y=-13195 $D=4
X34 GND supplyVDD NCAP_HSL130E_CDNS_6160560168413 $T=12475 -19555 0 0 $X=12315 $Y=-19975
X35 GND supplyVDD NCAP_HSL130E_CDNS_6160560168413 $T=22915 -19555 0 0 $X=22755 $Y=-19975
X36 GND supplyVDD NCAP_HSL130E_CDNS_6160560168413 $T=33355 -19555 0 0 $X=33195 $Y=-19975
X37 GND supplyVDD NCAP_HSL130E_CDNS_6160560168413 $T=43795 -19555 0 0 $X=43635 $Y=-19975
X38 GND supplyVDD NCAP_HSL130E_CDNS_6160560168413 $T=54245 -19555 0 0 $X=54085 $Y=-19975
X49 input GND supplyVDD 1 INV2 $T=8320 -13380 0 0 $X=9765 $Y=-20495
X50 21 GND supplyVDD 9 INV2 $T=18760 -13380 0 0 $X=20205 $Y=-20495
X51 22 GND supplyVDD 12 INV2 $T=29200 -13380 0 0 $X=30645 $Y=-20495
X52 div GND supplyVDD 13 INV2 $T=31360 -16010 1 0 $X=32805 $Y=-15010
X53 div1 GND supplyVDD 17 INV2 $T=38560 -16010 1 0 $X=40005 $Y=-15010
X54 23 GND supplyVDD 14 INV2 $T=39640 -13380 0 0 $X=41085 $Y=-20495
X55 24 GND supplyVDD 16 INV2 $T=50080 -13380 0 0 $X=51525 $Y=-20495
X56 GND 2 1 GND N_12_HSL130E_CDNS_616056016847 $T=10325 -7375 1 0 $X=9825 $Y=-8115
X57 rst_n 1 21 GND supplyVDD divideby2 $T=12485 -13320 0 0 $X=15165 $Y=-20495
X58 rst_n 9 22 GND supplyVDD divideby2 $T=22925 -13320 0 0 $X=25605 $Y=-20495
X59 rst_n 12 23 GND supplyVDD divideby2 $T=33365 -13320 0 0 $X=36045 $Y=-20495
X60 rst_n 14 24 GND supplyVDD divideby2 $T=43805 -13320 0 0 $X=46485 $Y=-20495
X61 10 output 17 GND N_12_HSL130E_CDNS_6160560168411 $T=43845 -9695 1 0 $X=43345 $Y=-10615
X62 output 15 div1 GND N_12_HSL130E_CDNS_6160560168411 $T=45365 -9695 0 180 $X=44745 $Y=-10615
X63 10 output div1 supplyVDD P_12_HSL130E_CDNS_6160560168412 $T=43845 -12235 1 0 $X=43205 $Y=-14215
X64 output 15 17 supplyVDD P_12_HSL130E_CDNS_6160560168412 $T=45365 -12235 0 180 $X=44605 $Y=-14215
X65 10 12 div GND N_12_HSL130E_CDNS_6160560168425 $T=37205 -9695 1 0 $X=36705 $Y=-10615
X66 15 16 div GND N_12_HSL130E_CDNS_6160560168425 $T=39125 -9695 1 0 $X=38625 $Y=-10615
X67 10 12 13 supplyVDD P_12_HSL130E_CDNS_6160560168423 $T=37205 -12235 1 0 $X=36565 $Y=-14215
X68 15 16 13 supplyVDD P_12_HSL130E_CDNS_6160560168423 $T=39125 -12235 1 0 $X=38485 $Y=-14215
X69 9 13 10 GND N_12_HSL130E_CDNS_6160560168424 $T=36685 -9695 1 0 $X=36185 $Y=-10615
X70 14 13 15 GND N_12_HSL130E_CDNS_6160560168424 $T=38605 -9695 1 0 $X=38105 $Y=-10615
X71 9 div 10 supplyVDD P_12_HSL130E_CDNS_6160560168426 $T=36685 -12235 1 0 $X=36045 $Y=-14215
X72 14 div 15 supplyVDD P_12_HSL130E_CDNS_6160560168426 $T=38605 -12235 1 0 $X=37965 $Y=-14215
.ENDS
***************************************
