* SPICE NETLIST
***************************************

.SUBCKT N_12_RF D G S B
.ENDS
***************************************
.SUBCKT P_12_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_33_RF D G S B
.ENDS
***************************************
.SUBCKT P_33_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_BPW_12_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT N_BPW_33_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT VARMIS_12_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARMIS_33_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARDIOP_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT RNNPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNPPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNHR_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MIMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT PAD_RF PLUS PSUB
.ENDS
***************************************
.SUBCKT DIOP_ESD_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT DIODN_ESD_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT NPN_SV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_SVL20_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NVL20_RF C B E S
.ENDS
***************************************
.SUBCKT PNP_NV50X50_RF C B E
.ENDS
***************************************
.SUBCKT PNP_NVL20_RF C B E
.ENDS
***************************************
.SUBCKT L_CR20K_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWCR20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_CR20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20KCT_RFVIL PLUS MINUS CT NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20KCT_RFVIL PLUS MINUS CT PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWSQSK_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SYMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP3_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP4_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT M1_POLY_CDNS_6160561533919
** N=2 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NCAP_HSL130E_CDNS_6160561533913 1 2
** N=2 EP=2 IP=0 FDC=1
M0 2 1 2 2 N_12_HSL130E L=2.1e-06 W=4e-06 AD=1.36e-12 AS=1.36e-12 PD=8.68e-06 PS=8.68e-06 sa=3.4e-07 sb=3.4e-07 $X=340 $Y=0 $D=3
.ENDS
***************************************
.SUBCKT M1_NWEL_CDNS_6160561533921
** N=2 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT M1_PACTIVE_CDNS_6160561533920
** N=1 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT INV2 in vss vdd out
** N=4 EP=4 IP=0 FDC=2
M0 out in vss vss N_12_HSL130E L=2.4e-07 W=4.8e-07 AD=1.632e-13 AS=1.632e-13 PD=1.64e-06 PS=1.64e-06 sa=3.4e-07 sb=3.4e-07 $X=2795 $Y=-6220 $D=3
M1 out in vdd vdd P_12_HSL130E L=2.4e-07 W=2.16e-06 AD=7.344e-13 AS=7.344e-13 PD=5e-06 PS=5e-06 sa=3.4e-07 sb=3.4e-07 $X=2795 $Y=-4090 $D=4
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_616056153397
** N=4 EP=0 IP=0 FDC=0
*.SEEDPROM
.ENDS
***************************************
.SUBCKT divideby2 rst_n in out vss vdd
** N=10 EP=5 IP=8 FDC=10
M0 10 in out vss N_12_HSL130E L=1.2e-07 W=3.2e-07 AD=4.16e-14 AS=1.088e-13 PD=5.8e-07 PS=1.32e-06 sa=3.4e-07 sb=1.24e-06 $X=4890 $Y=-5395 $D=3
M1 8 6 10 vss N_12_HSL130E L=1.2e-07 W=3.2e-07 AD=6.4e-14 AS=4.16e-14 PD=7.2e-07 PS=5.8e-07 sa=7.2e-07 sb=8.6e-07 $X=5270 $Y=-5395 $D=3
M2 7 out vss vss N_12_HSL130E L=1.2e-07 W=1.6e-07 AD=9.44e-14 AS=4.16e-14 PD=1.32e-06 PS=5.06667e-07 sa=9e-07 sb=3.8e-07 $X=6350 $Y=-6335 $D=3
M3 vss in 8 vss N_12_HSL130E L=1.2e-07 W=3.2e-07 AD=8.32e-14 AS=1.088e-13 PD=1.01333e-06 PS=1.32e-06 sa=3.4e-07 sb=5.11429e-07 $X=5790 $Y=-6435 $D=3
M4 6 7 8 vss N_12_HSL130E L=1.2e-07 W=3.2e-07 AD=1.088e-13 AS=6.4e-14 PD=1.32e-06 PS=7.2e-07 sa=1.24e-06 sb=3.4e-07 $X=5790 $Y=-5395 $D=3
M5 vdd rst_n out vdd P_12_HSL130E L=1.2e-07 W=1.6e-07 AD=9.44e-14 AS=9.44e-14 PD=1.32e-06 PS=1.32e-06 sa=3.8e-07 sb=3.8e-07 $X=3890 $Y=-2210 $D=4
M6 vdd in 6 vdd P_12_HSL130E L=1.2e-07 W=7.2e-07 AD=1.44e-13 AS=2.448e-13 PD=1.12e-06 PS=2.12e-06 sa=3.4e-07 sb=8.6e-07 $X=5330 $Y=-2710 $D=4
M7 out 6 vdd vdd P_12_HSL130E L=1.2e-07 W=7.2e-07 AD=2.448e-13 AS=1.44e-13 PD=2.12e-06 PS=1.12e-06 sa=8.6e-07 sb=3.4e-07 $X=5850 $Y=-2710 $D=4
M8 9 in 7 vdd P_12_HSL130E L=1.2e-07 W=1.44e-06 AD=1.872e-13 AS=4.896e-13 PD=1.7e-06 PS=3.56e-06 sa=3.4e-07 sb=7.2e-07 $X=7250 $Y=-3430 $D=4
M9 vdd out 9 vdd P_12_HSL130E L=1.2e-07 W=1.44e-06 AD=4.896e-13 AS=1.872e-13 PD=3.56e-06 PS=1.7e-06 sa=7.2e-07 sb=3.4e-07 $X=7630 $Y=-3430 $D=4
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_6160561533911 1 2 3 4
** N=4 EP=4 IP=0 FDC=1
M0 3 1 2 4 N_12_HSL130E L=1.2e-07 W=5e-07 AD=1.7e-13 AS=1.7e-13 PD=1.68e-06 PS=1.68e-06 sa=3.4e-07 sb=3.4e-07 $X=0 $Y=180 $D=3
.ENDS
***************************************
.SUBCKT P_12_HSL130E_CDNS_6160561533912 1 2 3 4
** N=5 EP=4 IP=0 FDC=1
M0 3 1 2 4 P_12_HSL130E L=1.2e-07 W=1.5e-06 AD=5.1e-13 AS=5.1e-13 PD=3.68e-06 PS=3.68e-06 sa=3.4e-07 sb=3.4e-07 $X=0 $Y=180 $D=4
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_6160561533925 1 2 3 4
** N=4 EP=4 IP=0 FDC=1
M0 3 1 2 4 N_12_HSL130E L=1.2e-07 W=5e-07 AD=1.7e-13 AS=1e-13 PD=1.68e-06 PS=9e-07 sa=8.6e-07 sb=3.4e-07 $X=0 $Y=180 $D=3
.ENDS
***************************************
.SUBCKT P_12_HSL130E_CDNS_6160561533923 1 2 3 4
** N=5 EP=4 IP=0 FDC=1
M0 3 1 2 4 P_12_HSL130E L=1.2e-07 W=1.5e-06 AD=5.1e-13 AS=3e-13 PD=3.68e-06 PS=1.9e-06 sa=8.6e-07 sb=3.4e-07 $X=0 $Y=180 $D=4
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_6160561533924 1 2 3 4
** N=4 EP=4 IP=0 FDC=1
M0 3 1 2 4 N_12_HSL130E L=1.2e-07 W=5e-07 AD=1e-13 AS=1.7e-13 PD=9e-07 PS=1.68e-06 sa=3.4e-07 sb=8.6e-07 $X=0 $Y=180 $D=3
.ENDS
***************************************
.SUBCKT P_12_HSL130E_CDNS_6160561533926 1 2 3 4
** N=5 EP=4 IP=0 FDC=1
M0 3 1 2 4 P_12_HSL130E L=1.2e-07 W=1.5e-06 AD=3e-13 AS=5.1e-13 PD=1.9e-06 PS=3.68e-06 sa=3.4e-07 sb=8.6e-07 $X=0 $Y=180 $D=4
.ENDS
***************************************
.SUBCKT division_core supplyVDD div div1 GND final output input rst_n
** N=25 EP=8 IP=141 FDC=101
M0 3 2 GND GND N_12_HSL130E L=1.2e-07 W=4.8e-07 AD=1.632e-13 AS=1.632e-13 PD=1.64e-06 PS=1.64e-06 sa=3.4e-07 sb=3.4e-07 $X=11445 $Y=-7875 $D=3
M1 4 3 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=4.896e-13 AS=4.896e-13 PD=3.56e-06 PS=3.56e-06 sa=3.4e-07 sb=3.4e-07 $X=12565 $Y=-7875 $D=3
M2 GND 4 5 GND N_12_HSL130E L=1.2e-07 W=1.15e-06 AD=2.3e-13 AS=3.91e-13 PD=1.55e-06 PS=2.98e-06 sa=3.4e-07 sb=6.1e-06 $X=13645 $Y=-7875 $D=3
M3 5 4 GND GND N_12_HSL130E L=1.2e-07 W=1.15e-06 AD=2.3e-13 AS=2.3e-13 PD=1.55e-06 PS=1.55e-06 sa=8.6e-07 sb=5.58e-06 $X=14165 $Y=-7875 $D=3
M4 GND 4 5 GND N_12_HSL130E L=1.2e-07 W=1.15e-06 AD=2.68452e-13 AS=2.3e-13 PD=1.6695e-06 PS=1.55e-06 sa=1.38e-06 sb=5.06e-06 $X=14685 $Y=-7875 $D=3
M5 final 5 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=2.88e-13 AS=3.36148e-13 PD=1.84e-06 PS=2.0905e-06 sa=1.04769e-06 sb=4.5e-06 $X=15245 $Y=-7875 $D=3
M6 GND 5 final GND N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=2.88e-13 AS=2.88e-13 PD=1.84e-06 PS=1.84e-06 sa=1.80633e-06 sb=3.98e-06 $X=15765 $Y=-7875 $D=3
M7 final 5 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=2.88e-13 AS=2.88e-13 PD=1.84e-06 PS=1.84e-06 sa=2.42414e-06 sb=3.46e-06 $X=16285 $Y=-7875 $D=3
M8 GND 5 final GND N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=2.88e-13 AS=2.88e-13 PD=1.84e-06 PS=1.84e-06 sa=2.99737e-06 sb=2.94e-06 $X=16805 $Y=-7875 $D=3
M9 final 5 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=2.88e-13 AS=2.88e-13 PD=1.84e-06 PS=1.84e-06 sa=3.55085e-06 sb=2.42e-06 $X=17325 $Y=-7875 $D=3
M10 GND 5 final GND N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=2.88e-13 AS=2.88e-13 PD=1.84e-06 PS=1.84e-06 sa=4.09385e-06 sb=1.9e-06 $X=17845 $Y=-7875 $D=3
M11 final 5 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=2.88e-13 AS=2.88e-13 PD=1.84e-06 PS=1.84e-06 sa=4.63062e-06 sb=1.38e-06 $X=18365 $Y=-7875 $D=3
M12 GND 5 final GND N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=2.88e-13 AS=2.88e-13 PD=1.84e-06 PS=1.84e-06 sa=5.16339e-06 sb=8.6e-07 $X=18885 $Y=-7875 $D=3
M13 final 5 GND GND N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=4.896e-13 AS=2.88e-13 PD=3.56e-06 PS=1.84e-06 sa=5.69345e-06 sb=3.4e-07 $X=19405 $Y=-7875 $D=3
M14 2 1 GND GND N_12_HSL130E L=1.2e-07 W=3.2e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 sa=3.4e-07 sb=3.4e-07 $X=10325 $Y=-7875 $D=3
M15 2 1 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=9.6e-07 AD=3.264e-13 AS=3.264e-13 PD=2.6e-06 PS=2.6e-06 sa=3.4e-07 sb=3.4e-07 $X=10325 $Y=-9835 $D=4
M16 3 2 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=1.44e-06 AD=4.896e-13 AS=4.896e-13 PD=3.56e-06 PS=3.56e-06 sa=3.4e-07 sb=3.4e-07 $X=11445 $Y=-10315 $D=4
M17 4 3 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=3.455e-06 AD=1.1747e-12 AS=1.1747e-12 PD=7.59e-06 PS=7.59e-06 sa=3.4e-07 sb=3.4e-07 $X=12565 $Y=-12330 $D=4
M18 supplyVDD 4 5 supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 AD=8.64e-13 AS=1.4688e-12 PD=4.72e-06 PS=9.32e-06 sa=3.4e-07 sb=6.06e-06 $X=13685 $Y=-13195 $D=4
M19 5 4 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 AD=8.64e-13 AS=8.64e-13 PD=4.72e-06 PS=4.72e-06 sa=8.6e-07 sb=5.54e-06 $X=14205 $Y=-13195 $D=4
M20 supplyVDD 4 5 supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 AD=8.64e-13 AS=8.64e-13 PD=4.72e-06 PS=4.72e-06 sa=1.38e-06 sb=5.02e-06 $X=14725 $Y=-13195 $D=4
M21 final 5 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 AD=8.64e-13 AS=8.64e-13 PD=4.72e-06 PS=4.72e-06 sa=1.9e-06 sb=4.5e-06 $X=15245 $Y=-13195 $D=4
M22 supplyVDD 5 final supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 AD=8.64e-13 AS=8.64e-13 PD=4.72e-06 PS=4.72e-06 sa=2.42e-06 sb=3.98e-06 $X=15765 $Y=-13195 $D=4
M23 final 5 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 AD=8.64e-13 AS=8.64e-13 PD=4.72e-06 PS=4.72e-06 sa=2.94e-06 sb=3.46e-06 $X=16285 $Y=-13195 $D=4
M24 supplyVDD 5 final supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 AD=8.64e-13 AS=8.64e-13 PD=4.72e-06 PS=4.72e-06 sa=3.46e-06 sb=2.94e-06 $X=16805 $Y=-13195 $D=4
M25 final 5 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 AD=8.64e-13 AS=8.64e-13 PD=4.72e-06 PS=4.72e-06 sa=3.98e-06 sb=2.42e-06 $X=17325 $Y=-13195 $D=4
M26 supplyVDD 5 final supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 AD=8.64e-13 AS=8.64e-13 PD=4.72e-06 PS=4.72e-06 sa=4.5e-06 sb=1.9e-06 $X=17845 $Y=-13195 $D=4
M27 final 5 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 AD=8.64e-13 AS=8.64e-13 PD=4.72e-06 PS=4.72e-06 sa=5.02e-06 sb=1.38e-06 $X=18365 $Y=-13195 $D=4
M28 supplyVDD 5 final supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 AD=8.64e-13 AS=8.64e-13 PD=4.72e-06 PS=4.72e-06 sa=5.54e-06 sb=8.6e-07 $X=18885 $Y=-13195 $D=4
M29 final 5 supplyVDD supplyVDD P_12_HSL130E L=1.2e-07 W=4.32e-06 AD=1.4688e-12 AS=8.64e-13 PD=9.32e-06 PS=4.72e-06 sa=6.06e-06 sb=3.4e-07 $X=19405 $Y=-13195 $D=4
X35 supplyVDD GND NCAP_HSL130E_CDNS_6160561533913 $T=12475 -19555 0 0 $X=12315 $Y=-19975
X36 supplyVDD GND NCAP_HSL130E_CDNS_6160561533913 $T=22915 -19555 0 0 $X=22755 $Y=-19975
X37 supplyVDD GND NCAP_HSL130E_CDNS_6160561533913 $T=33355 -19555 0 0 $X=33195 $Y=-19975
X38 supplyVDD GND NCAP_HSL130E_CDNS_6160561533913 $T=43795 -19555 0 0 $X=43635 $Y=-19975
X39 supplyVDD GND NCAP_HSL130E_CDNS_6160561533913 $T=54245 -19555 0 0 $X=54085 $Y=-19975
X50 input GND supplyVDD 1 INV2 $T=8320 -13380 0 0 $X=9765 $Y=-20495
X51 21 GND supplyVDD 13 INV2 $T=18760 -13380 0 0 $X=20205 $Y=-20495
X52 22 GND supplyVDD 15 INV2 $T=29200 -13380 0 0 $X=30645 $Y=-20495
X53 div GND supplyVDD 7 INV2 $T=31360 -16010 1 0 $X=32805 $Y=-15010
X54 div1 GND supplyVDD 10 INV2 $T=38560 -16010 1 0 $X=40005 $Y=-15010
X55 23 GND supplyVDD 16 INV2 $T=39640 -13380 0 0 $X=41085 $Y=-20495
X56 24 GND supplyVDD 18 INV2 $T=50080 -13380 0 0 $X=51525 $Y=-20495
X58 rst_n 1 21 GND supplyVDD divideby2 $T=12485 -13320 0 0 $X=15165 $Y=-20495
X59 rst_n 13 22 GND supplyVDD divideby2 $T=22925 -13320 0 0 $X=25605 $Y=-20495
X60 rst_n 15 23 GND supplyVDD divideby2 $T=33365 -13320 0 0 $X=36045 $Y=-20495
X61 rst_n 16 24 GND supplyVDD divideby2 $T=43805 -13320 0 0 $X=46485 $Y=-20495
X62 10 14 output GND N_12_HSL130E_CDNS_6160561533911 $T=43845 -9695 1 0 $X=43345 $Y=-10615
X63 div1 output 17 GND N_12_HSL130E_CDNS_6160561533911 $T=45365 -9695 0 180 $X=44745 $Y=-10615
X64 div1 14 output supplyVDD P_12_HSL130E_CDNS_6160561533912 $T=43845 -12235 1 0 $X=43205 $Y=-14215
X65 10 output 17 supplyVDD P_12_HSL130E_CDNS_6160561533912 $T=45365 -12235 0 180 $X=44605 $Y=-14215
X66 div 14 15 GND N_12_HSL130E_CDNS_6160561533925 $T=37205 -9695 1 0 $X=36705 $Y=-10615
X67 div 17 18 GND N_12_HSL130E_CDNS_6160561533925 $T=39125 -9695 1 0 $X=38625 $Y=-10615
X68 7 14 15 supplyVDD P_12_HSL130E_CDNS_6160561533923 $T=37205 -12235 1 0 $X=36565 $Y=-14215
X69 7 17 18 supplyVDD P_12_HSL130E_CDNS_6160561533923 $T=39125 -12235 1 0 $X=38485 $Y=-14215
X70 7 13 14 GND N_12_HSL130E_CDNS_6160561533924 $T=36685 -9695 1 0 $X=36185 $Y=-10615
X71 7 16 17 GND N_12_HSL130E_CDNS_6160561533924 $T=38605 -9695 1 0 $X=38105 $Y=-10615
X72 div 13 14 supplyVDD P_12_HSL130E_CDNS_6160561533926 $T=36685 -12235 1 0 $X=36045 $Y=-14215
X73 div 16 17 supplyVDD P_12_HSL130E_CDNS_6160561533926 $T=38605 -12235 1 0 $X=37965 $Y=-14215
.ENDS
***************************************
