* SPICE NETLIST
***************************************

.SUBCKT N_12_RF D G S B
.ENDS
***************************************
.SUBCKT P_12_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_33_RF D G S B
.ENDS
***************************************
.SUBCKT P_33_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_BPW_12_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT N_BPW_33_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT VARMIS_12_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARMIS_33_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARDIOP_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT RNNPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNPPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNHR_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MIMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT PAD_RF PLUS PSUB
.ENDS
***************************************
.SUBCKT DIOP_ESD_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT DIODN_ESD_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT NPN_SV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_SVL20_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NVL20_RF C B E S
.ENDS
***************************************
.SUBCKT PNP_NV50X50_RF C B E
.ENDS
***************************************
.SUBCKT PNP_NVL20_RF C B E
.ENDS
***************************************
.SUBCKT L_CR20K_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWCR20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_CR20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20KCT_RFVIL PLUS MINUS CT NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20KCT_RFVIL PLUS MINUS CT PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWSQSK_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SYMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP3_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP4_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT P_12_HSL130E_CDNS_616149366759 1 2 3
** N=4 EP=3 IP=0 FDC=6
M0 2 3 1 1 P_12_HSL130E L=1.2e-07 W=1.6e-06 $X=0 $Y=180 $D=4
M1 1 3 2 1 P_12_HSL130E L=1.2e-07 W=1.6e-06 $X=520 $Y=180 $D=4
M2 2 3 1 1 P_12_HSL130E L=1.2e-07 W=1.6e-06 $X=1040 $Y=180 $D=4
M3 1 3 2 1 P_12_HSL130E L=1.2e-07 W=1.6e-06 $X=1560 $Y=180 $D=4
M4 2 3 1 1 P_12_HSL130E L=1.2e-07 W=1.6e-06 $X=2080 $Y=180 $D=4
M5 1 3 2 1 P_12_HSL130E L=1.2e-07 W=1.6e-06 $X=2600 $Y=180 $D=4
.ENDS
***************************************
.SUBCKT INV2 in vss vdd out
** N=4 EP=4 IP=0 FDC=2
M0 out in vss vss N_12_HSL130E L=2.4e-07 W=4.8e-07 $X=2795 $Y=-6220 $D=3
M1 out in vdd vdd P_12_HSL130E L=2.4e-07 W=2.16e-06 $X=2795 $Y=-4090 $D=4
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_616149366755 1 2 3 4
** N=4 EP=4 IP=0 FDC=5
M0 2 3 1 4 N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=0 $Y=180 $D=3
M1 1 3 2 4 N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=520 $Y=180 $D=3
M2 2 3 1 4 N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=1040 $Y=180 $D=3
M3 1 3 2 4 N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=1560 $Y=180 $D=3
M4 2 3 1 4 N_12_HSL130E L=1.2e-07 W=1.44e-06 $X=2080 $Y=180 $D=3
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_616149366754 1 2 3 4
** N=4 EP=4 IP=0 FDC=1
M0 2 3 1 4 N_12_HSL130E L=1.2e-07 W=5e-07 $X=0 $Y=180 $D=3
.ENDS
***************************************
.SUBCKT P_12_HSL130E_CDNS_6161493667510 1 2 3 4
** N=5 EP=4 IP=0 FDC=2
M0 2 3 1 4 P_12_HSL130E L=1.2e-07 W=7.5e-07 $X=0 $Y=180 $D=4
M1 1 3 2 4 P_12_HSL130E L=1.2e-07 W=7.5e-07 $X=520 $Y=180 $D=4
.ENDS
***************************************
.SUBCKT M1_POLY_CDNS_6161493667521
** N=2 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT P_12_HSL130E_CDNS_616149366758 1 2 3
** N=4 EP=3 IP=0 FDC=1
M0 1 2 1 3 P_12_HSL130E L=4e-06 W=4e-06 $X=0 $Y=180 $D=4
.ENDS
***************************************
.SUBCKT INV_OSC in out vss vdd
** N=4 EP=4 IP=0 FDC=4
M0 out in vss vss N_12_HSL130E L=2.65e-07 W=3.6e-07 $X=2010 $Y=-6005 $D=3
M1 vss in out vss N_12_HSL130E L=2.65e-07 W=3.6e-07 $X=2675 $Y=-6005 $D=3
M2 out in vdd vdd P_12_HSL130E L=2.65e-07 W=1.62e-06 $X=2010 $Y=-3335 $D=4
M3 vdd in out vdd P_12_HSL130E L=2.65e-07 W=1.62e-06 $X=2675 $Y=-3335 $D=4
.ENDS
***************************************
.SUBCKT ringOsc_HS vctrl f_sel vss start_n vdd out
** N=27 EP=6 IP=221 FDC=122
M0 25 start_n vss vss N_12_HSL130E L=2.4e-07 W=1.6e-07 $X=142840 $Y=-9040 $D=3
M1 25 start_n vdd vdd P_12_HSL130E L=2.4e-07 W=7.2e-07 $X=142880 $Y=-5275 $D=4
X2 vdd out 25 P_12_HSL130E_CDNS_616149366759 $T=146290 -8415 0 0 $X=145650 $Y=-8535
X3 vdd out 25 P_12_HSL130E_CDNS_616149366759 $T=146290 -6335 0 0 $X=145650 $Y=-6455
X4 f_sel vss vdd 6 INV2 $T=24435 -2625 0 0 $X=26570 $Y=-9400
X5 1 vss vdd out INV2 $T=141605 -2625 0 0 $X=143740 $Y=-9400
X6 1 vss start_n vss N_12_HSL130E_CDNS_616149366755 $T=139360 -9280 0 0 $X=138860 $Y=-9340
X7 vss 1 start_n vss N_12_HSL130E_CDNS_616149366755 $T=141560 -7360 1 180 $X=138860 $Y=-7420
X8 7 5 6 vss N_12_HSL130E_CDNS_616149366754 $T=29390 -9280 1 180 $X=28770 $Y=-9340
X9 1 22 6 vss N_12_HSL130E_CDNS_616149366754 $T=136170 -9280 1 180 $X=135550 $Y=-9340
X10 1 5 f_sel vss N_12_HSL130E_CDNS_616149366754 $T=137450 -9280 0 0 $X=136950 $Y=-9340
X11 7 5 f_sel vdd P_12_HSL130E_CDNS_6161493667510 $T=28750 -5485 0 0 $X=28110 $Y=-5605
X12 1 22 f_sel vdd P_12_HSL130E_CDNS_6161493667510 $T=135530 -5485 0 0 $X=134890 $Y=-5605
X13 1 5 6 vdd P_12_HSL130E_CDNS_6161493667510 $T=137450 -5485 0 0 $X=136810 $Y=-5605
X31 3 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=6970 -8735 0 0 $X=6330 $Y=-8855
X32 4 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=14460 -8735 0 0 $X=13820 $Y=-8855
X33 5 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=21950 -8735 0 0 $X=21310 $Y=-8855
X34 9 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=32880 -8735 0 0 $X=32240 $Y=-8855
X35 10 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=40370 -8735 0 0 $X=39730 $Y=-8855
X36 11 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=47860 -8735 0 0 $X=47220 $Y=-8855
X37 12 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=55350 -8735 0 0 $X=54710 $Y=-8855
X38 13 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=62840 -8735 0 0 $X=62200 $Y=-8855
X39 14 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=70330 -8735 0 0 $X=69690 $Y=-8855
X40 15 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=77820 -8735 0 0 $X=77180 $Y=-8855
X41 16 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=85310 -8735 0 0 $X=84670 $Y=-8855
X42 17 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=92800 -8735 0 0 $X=92160 $Y=-8855
X43 18 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=100290 -8735 0 0 $X=99650 $Y=-8855
X44 19 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=107780 -8735 0 0 $X=107140 $Y=-8855
X45 20 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=115270 -8735 0 0 $X=114630 $Y=-8855
X46 21 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=122760 -8735 0 0 $X=122120 $Y=-8855
X47 22 vctrl vdd P_12_HSL130E_CDNS_616149366758 $T=130250 -8735 0 0 $X=129610 $Y=-8855
X48 1 3 vss vdd INV_OSC $T=2750 -2840 0 0 $X=4120 $Y=-9680
X49 3 4 vss vdd INV_OSC $T=10240 -2840 0 0 $X=11610 $Y=-9680
X50 4 5 vss vdd INV_OSC $T=17730 -2840 0 0 $X=19100 $Y=-9680
X51 7 9 vss vdd INV_OSC $T=28660 -2840 0 0 $X=30030 $Y=-9680
X52 9 10 vss vdd INV_OSC $T=36150 -2840 0 0 $X=37520 $Y=-9680
X53 10 11 vss vdd INV_OSC $T=43640 -2840 0 0 $X=45010 $Y=-9680
X54 11 12 vss vdd INV_OSC $T=51130 -2840 0 0 $X=52500 $Y=-9680
X55 12 13 vss vdd INV_OSC $T=58620 -2840 0 0 $X=59990 $Y=-9680
X56 13 14 vss vdd INV_OSC $T=66110 -2840 0 0 $X=67480 $Y=-9680
X57 14 15 vss vdd INV_OSC $T=73600 -2840 0 0 $X=74970 $Y=-9680
X58 15 16 vss vdd INV_OSC $T=81090 -2840 0 0 $X=82460 $Y=-9680
X59 16 17 vss vdd INV_OSC $T=88580 -2840 0 0 $X=89950 $Y=-9680
X60 17 18 vss vdd INV_OSC $T=96070 -2840 0 0 $X=97440 $Y=-9680
X61 18 19 vss vdd INV_OSC $T=103560 -2840 0 0 $X=104930 $Y=-9680
X62 19 20 vss vdd INV_OSC $T=111050 -2840 0 0 $X=112420 $Y=-9680
X63 20 21 vss vdd INV_OSC $T=118540 -2840 0 0 $X=119910 $Y=-9680
X64 21 22 vss vdd INV_OSC $T=126030 -2840 0 0 $X=127400 $Y=-9680
.ENDS
***************************************
