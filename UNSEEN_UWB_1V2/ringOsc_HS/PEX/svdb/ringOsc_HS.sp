* SPICE NETLIST
***************************************

.SUBCKT N_12_RF D G S B
.ENDS
***************************************
.SUBCKT P_12_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_33_RF D G S B
.ENDS
***************************************
.SUBCKT P_33_RF D G S B PSUB
.ENDS
***************************************
.SUBCKT N_BPW_12_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT N_BPW_33_RF D G S B NW PSUB
.ENDS
***************************************
.SUBCKT VARMIS_12_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARMIS_33_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT VARDIOP_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT RNNPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNPPO_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT RNHR_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MIMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_MM PLUS MINUS B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASY_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT PAD_RF PLUS PSUB
.ENDS
***************************************
.SUBCKT DIOP_ESD_RF PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT DIODN_ESD_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT NPN_SV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_SVL20_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NV50X50_RF C B E S
.ENDS
***************************************
.SUBCKT NPN_NVL20_RF C B E S
.ENDS
***************************************
.SUBCKT PNP_NV50X50_RF C B E
.ENDS
***************************************
.SUBCKT PNP_NVL20_RF C B E
.ENDS
***************************************
.SUBCKT L_CR20K_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWCR20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_CR20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20KCT_RFVIL PLUS MINUS CT NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20KCT_RFVIL PLUS MINUS CT PSUB
.ENDS
***************************************
.SUBCKT L_NWSY20K_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SY20K_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_NWSQSK_RFVIL PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT L_SQSK_RFVIL PLUS MINUS PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_SYMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ASMESH_MM PLUS1 MINUS1 PLUS2 MINUS2 B
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP3_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT MOMCAPS_ARRAY_VP4_RF PLUS MINUS NW PSUB
.ENDS
***************************************
.SUBCKT P_12_HSL130E_CDNS_616150083429 1 2 3
** N=4 EP=3 IP=0 FDC=6
M0 3 1 2 2 P_12_HSL130E L=1.2e-07 W=1.6e-06 AD=3.2e-13 AS=5.44e-13 PD=2e-06 PS=3.88e-06 sa=3.4e-07 sb=2.94e-06 $X=0 $Y=180 $D=4
M1 2 1 3 2 P_12_HSL130E L=1.2e-07 W=1.6e-06 AD=3.2e-13 AS=3.2e-13 PD=2e-06 PS=2e-06 sa=8.6e-07 sb=2.42e-06 $X=520 $Y=180 $D=4
M2 3 1 2 2 P_12_HSL130E L=1.2e-07 W=1.6e-06 AD=3.2e-13 AS=3.2e-13 PD=2e-06 PS=2e-06 sa=1.38e-06 sb=1.9e-06 $X=1040 $Y=180 $D=4
M3 2 1 3 2 P_12_HSL130E L=1.2e-07 W=1.6e-06 AD=3.2e-13 AS=3.2e-13 PD=2e-06 PS=2e-06 sa=1.9e-06 sb=1.38e-06 $X=1560 $Y=180 $D=4
M4 3 1 2 2 P_12_HSL130E L=1.2e-07 W=1.6e-06 AD=3.2e-13 AS=3.2e-13 PD=2e-06 PS=2e-06 sa=2.42e-06 sb=8.6e-07 $X=2080 $Y=180 $D=4
M5 2 1 3 2 P_12_HSL130E L=1.2e-07 W=1.6e-06 AD=5.44e-13 AS=3.2e-13 PD=3.88e-06 PS=2e-06 sa=2.94e-06 sb=3.4e-07 $X=2600 $Y=180 $D=4
.ENDS
***************************************
.SUBCKT INV2 in vss vdd out
** N=4 EP=4 IP=0 FDC=2
M0 out in vss vss N_12_HSL130E L=2.4e-07 W=4.8e-07 AD=1.632e-13 AS=1.632e-13 PD=1.64e-06 PS=1.64e-06 sa=3.4e-07 sb=3.4e-07 $X=2795 $Y=-6220 $D=3
M1 out in vdd vdd P_12_HSL130E L=2.4e-07 W=2.16e-06 AD=7.344e-13 AS=7.344e-13 PD=5e-06 PS=5e-06 sa=3.4e-07 sb=3.4e-07 $X=2795 $Y=-4090 $D=4
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_616150083425 1 2 3 4
** N=4 EP=4 IP=0 FDC=5
M0 3 1 2 4 N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=2.88e-13 AS=4.896e-13 PD=1.84e-06 PS=3.56e-06 sa=3.4e-07 sb=2.42e-06 $X=0 $Y=180 $D=3
M1 2 1 3 4 N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=2.88e-13 AS=2.88e-13 PD=1.84e-06 PS=1.84e-06 sa=8.6e-07 sb=1.9e-06 $X=520 $Y=180 $D=3
M2 3 1 2 4 N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=2.88e-13 AS=2.88e-13 PD=1.84e-06 PS=1.84e-06 sa=1.38e-06 sb=1.38e-06 $X=1040 $Y=180 $D=3
M3 2 1 3 4 N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=2.88e-13 AS=2.88e-13 PD=1.84e-06 PS=1.84e-06 sa=1.9e-06 sb=8.6e-07 $X=1560 $Y=180 $D=3
M4 3 1 2 4 N_12_HSL130E L=1.2e-07 W=1.44e-06 AD=4.896e-13 AS=2.88e-13 PD=3.56e-06 PS=1.84e-06 sa=2.42e-06 sb=3.4e-07 $X=2080 $Y=180 $D=3
.ENDS
***************************************
.SUBCKT N_12_HSL130E_CDNS_616150083424 1 2 3 4
** N=4 EP=4 IP=0 FDC=1
M0 3 1 2 4 N_12_HSL130E L=1.2e-07 W=5e-07 AD=1.7e-13 AS=1.7e-13 PD=1.68e-06 PS=1.68e-06 sa=3.4e-07 sb=3.4e-07 $X=0 $Y=180 $D=3
.ENDS
***************************************
.SUBCKT P_12_HSL130E_CDNS_6161500834210 1 2 3 4
** N=5 EP=4 IP=0 FDC=2
M0 3 1 2 4 P_12_HSL130E L=1.2e-07 W=7.5e-07 AD=1.5e-13 AS=2.55e-13 PD=1.15e-06 PS=2.18e-06 sa=3.4e-07 sb=8.6e-07 $X=0 $Y=180 $D=4
M1 2 1 3 4 P_12_HSL130E L=1.2e-07 W=7.5e-07 AD=2.55e-13 AS=1.5e-13 PD=2.18e-06 PS=1.15e-06 sa=8.6e-07 sb=3.4e-07 $X=520 $Y=180 $D=4
.ENDS
***************************************
.SUBCKT M1_POLY_CDNS_6161500834221
** N=2 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT P_12_HSL130E_CDNS_616150083428 1 2 3
** N=4 EP=3 IP=0 FDC=1
M0 2 1 2 3 P_12_HSL130E L=4e-06 W=4e-06 AD=1.36e-12 AS=1.36e-12 PD=8.68e-06 PS=8.68e-06 sa=3.4e-07 sb=3.4e-07 $X=0 $Y=180 $D=4
.ENDS
***************************************
.SUBCKT INV_OSC in out vss vdd
** N=4 EP=4 IP=0 FDC=4
M0 out in vss vss N_12_HSL130E L=2.65e-07 W=3.6e-07 AD=7.2e-14 AS=1.224e-13 PD=7.6e-07 PS=1.4e-06 sa=3.4e-07 sb=1.005e-06 $X=2010 $Y=-6005 $D=3
M1 vss in out vss N_12_HSL130E L=2.65e-07 W=3.6e-07 AD=1.224e-13 AS=7.2e-14 PD=1.4e-06 PS=7.6e-07 sa=1.005e-06 sb=3.4e-07 $X=2675 $Y=-6005 $D=3
M2 out in vdd vdd P_12_HSL130E L=2.65e-07 W=1.62e-06 AD=3.24e-13 AS=5.508e-13 PD=2.02e-06 PS=3.92e-06 sa=3.4e-07 sb=1.005e-06 $X=2010 $Y=-3335 $D=4
M3 vdd in out vdd P_12_HSL130E L=2.65e-07 W=1.62e-06 AD=5.508e-13 AS=3.24e-13 PD=3.92e-06 PS=2.02e-06 sa=1.005e-06 sb=3.4e-07 $X=2675 $Y=-3335 $D=4
.ENDS
***************************************
.SUBCKT ringOsc_HS start_n vctrl f_sel vss vdd out
** N=27 EP=6 IP=221 FDC=122
M0 5 start_n vss vss N_12_HSL130E L=2.4e-07 W=1.6e-07 AD=9.44e-14 AS=9.44e-14 PD=1.32e-06 PS=1.32e-06 sa=3.8e-07 sb=3.8e-07 $X=142840 $Y=-9040 $D=3
M1 5 start_n vdd vdd P_12_HSL130E L=2.4e-07 W=7.2e-07 AD=2.448e-13 AS=2.448e-13 PD=2.12e-06 PS=2.12e-06 sa=3.4e-07 sb=3.4e-07 $X=142880 $Y=-5275 $D=4
X2 5 vdd out P_12_HSL130E_CDNS_616150083429 $T=146290 -8415 0 0 $X=145650 $Y=-8535
X3 5 vdd out P_12_HSL130E_CDNS_616150083429 $T=146290 -6335 0 0 $X=145650 $Y=-6455
X4 f_sel vss vdd 4 INV2 $T=24435 -2625 0 0 $X=26570 $Y=-9400
X5 6 vss vdd out INV2 $T=141605 -2625 0 0 $X=143740 $Y=-9400
X6 start_n 6 vss vss N_12_HSL130E_CDNS_616150083425 $T=139360 -9280 0 0 $X=138860 $Y=-9340
X7 start_n vss 6 vss N_12_HSL130E_CDNS_616150083425 $T=141560 -7360 1 180 $X=138860 $Y=-7420
X8 4 10 9 vss N_12_HSL130E_CDNS_616150083424 $T=29390 -9280 1 180 $X=28770 $Y=-9340
X9 4 6 24 vss N_12_HSL130E_CDNS_616150083424 $T=136170 -9280 1 180 $X=135550 $Y=-9340
X10 f_sel 6 9 vss N_12_HSL130E_CDNS_616150083424 $T=137450 -9280 0 0 $X=136950 $Y=-9340
X11 f_sel 10 9 vdd P_12_HSL130E_CDNS_6161500834210 $T=28750 -5485 0 0 $X=28110 $Y=-5605
X12 f_sel 6 24 vdd P_12_HSL130E_CDNS_6161500834210 $T=135530 -5485 0 0 $X=134890 $Y=-5605
X13 4 6 9 vdd P_12_HSL130E_CDNS_6161500834210 $T=137450 -5485 0 0 $X=136810 $Y=-5605
X31 vctrl 7 vdd P_12_HSL130E_CDNS_616150083428 $T=6970 -8735 0 0 $X=6330 $Y=-8855
X32 vctrl 8 vdd P_12_HSL130E_CDNS_616150083428 $T=14460 -8735 0 0 $X=13820 $Y=-8855
X33 vctrl 9 vdd P_12_HSL130E_CDNS_616150083428 $T=21950 -8735 0 0 $X=21310 $Y=-8855
X34 vctrl 11 vdd P_12_HSL130E_CDNS_616150083428 $T=32880 -8735 0 0 $X=32240 $Y=-8855
X35 vctrl 12 vdd P_12_HSL130E_CDNS_616150083428 $T=40370 -8735 0 0 $X=39730 $Y=-8855
X36 vctrl 13 vdd P_12_HSL130E_CDNS_616150083428 $T=47860 -8735 0 0 $X=47220 $Y=-8855
X37 vctrl 14 vdd P_12_HSL130E_CDNS_616150083428 $T=55350 -8735 0 0 $X=54710 $Y=-8855
X38 vctrl 15 vdd P_12_HSL130E_CDNS_616150083428 $T=62840 -8735 0 0 $X=62200 $Y=-8855
X39 vctrl 16 vdd P_12_HSL130E_CDNS_616150083428 $T=70330 -8735 0 0 $X=69690 $Y=-8855
X40 vctrl 17 vdd P_12_HSL130E_CDNS_616150083428 $T=77820 -8735 0 0 $X=77180 $Y=-8855
X41 vctrl 18 vdd P_12_HSL130E_CDNS_616150083428 $T=85310 -8735 0 0 $X=84670 $Y=-8855
X42 vctrl 19 vdd P_12_HSL130E_CDNS_616150083428 $T=92800 -8735 0 0 $X=92160 $Y=-8855
X43 vctrl 20 vdd P_12_HSL130E_CDNS_616150083428 $T=100290 -8735 0 0 $X=99650 $Y=-8855
X44 vctrl 21 vdd P_12_HSL130E_CDNS_616150083428 $T=107780 -8735 0 0 $X=107140 $Y=-8855
X45 vctrl 22 vdd P_12_HSL130E_CDNS_616150083428 $T=115270 -8735 0 0 $X=114630 $Y=-8855
X46 vctrl 23 vdd P_12_HSL130E_CDNS_616150083428 $T=122760 -8735 0 0 $X=122120 $Y=-8855
X47 vctrl 24 vdd P_12_HSL130E_CDNS_616150083428 $T=130250 -8735 0 0 $X=129610 $Y=-8855
X48 6 7 vss vdd INV_OSC $T=2750 -2840 0 0 $X=4120 $Y=-9680
X49 7 8 vss vdd INV_OSC $T=10240 -2840 0 0 $X=11610 $Y=-9680
X50 8 9 vss vdd INV_OSC $T=17730 -2840 0 0 $X=19100 $Y=-9680
X51 10 11 vss vdd INV_OSC $T=28660 -2840 0 0 $X=30030 $Y=-9680
X52 11 12 vss vdd INV_OSC $T=36150 -2840 0 0 $X=37520 $Y=-9680
X53 12 13 vss vdd INV_OSC $T=43640 -2840 0 0 $X=45010 $Y=-9680
X54 13 14 vss vdd INV_OSC $T=51130 -2840 0 0 $X=52500 $Y=-9680
X55 14 15 vss vdd INV_OSC $T=58620 -2840 0 0 $X=59990 $Y=-9680
X56 15 16 vss vdd INV_OSC $T=66110 -2840 0 0 $X=67480 $Y=-9680
X57 16 17 vss vdd INV_OSC $T=73600 -2840 0 0 $X=74970 $Y=-9680
X58 17 18 vss vdd INV_OSC $T=81090 -2840 0 0 $X=82460 $Y=-9680
X59 18 19 vss vdd INV_OSC $T=88580 -2840 0 0 $X=89950 $Y=-9680
X60 19 20 vss vdd INV_OSC $T=96070 -2840 0 0 $X=97440 $Y=-9680
X61 20 21 vss vdd INV_OSC $T=103560 -2840 0 0 $X=104930 $Y=-9680
X62 21 22 vss vdd INV_OSC $T=111050 -2840 0 0 $X=112420 $Y=-9680
X63 22 23 vss vdd INV_OSC $T=118540 -2840 0 0 $X=119910 $Y=-9680
X64 23 24 vss vdd INV_OSC $T=126030 -2840 0 0 $X=127400 $Y=-9680
.ENDS
***************************************
