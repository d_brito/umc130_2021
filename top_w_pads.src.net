************************************************************************
* auCdl Netlist:
* 
* Library Name:  UNSEEN_UWB_1V2
* Top Cell Name: top_w_pads
* View Name:     schematic
* Netlisted on:  Apr 29 12:39:11 2021
************************************************************************

.INCLUDE  /opt/pdk/umc13mmrf/CDL_include_file.cir
*.EQUATION
*.SCALE METER
*.MEGA
.PARAM



************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    INV2
* View Name:    schematic
************************************************************************

.SUBCKT INV2 in out vdd vss
*.PININFO in:B out:B vdd:B vss:B
MPM1 out in vdd vdd P_12_HSL130E W=2.16e-6 L=240.0n M=1.0
MNM0 out in vss vss N_12_HSL130E W=480e-9 L=240.0n M=1.0
.ENDS

************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    divideby2
* View Name:    schematic
************************************************************************

.SUBCKT divideby2 in out rst_n vdd vss
*.PININFO in:I out:I rst_n:I vdd:I vss:I
MNM77 out in net31 vss N_12_HSL130E W=320e-9 L=120.0n M=1.0
MNM74 net18 net13 net26 vss N_12_HSL130E W=320e-9 L=120.0n M=1.0
MNM72 net13 out vss vss N_12_HSL130E W=160.0n L=120.0n M=1.0
MNM75 net26 in vss vss N_12_HSL130E W=320e-9 L=120.0n M=1.0
MNM76 net31 net18 net26 vss N_12_HSL130E W=320e-9 L=120.0n M=1.0
MPM83 net32 out vdd vdd P_12_HSL130E W=1.44e-6 L=120.0n M=1.0
MPM84 out net18 vdd vdd P_12_HSL130E W=720.0n L=120.0n M=1.0
MPM79 net13 in net32 vdd P_12_HSL130E W=1.44e-6 L=120.0n M=1.0
MPM82 net18 in vdd vdd P_12_HSL130E W=720.0n L=120.0n M=1.0
MPM2 out rst_n vdd vdd P_12_HSL130E W=160.0n L=120.0n M=1.0
.ENDS

************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    tg
* View Name:    schematic
************************************************************************

.SUBCKT tg en ep in out vdd vss
*.PININFO en:I ep:I in:B out:B vdd:B vss:B
MM0 in en out vss N_12_HSL130E W=500n L=120.0n M=1.0
MM1 in ep out vdd P_12_HSL130E W=1.5u L=120.0n M=1.0
.ENDS

************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    division_core
* View Name:    schematic
************************************************************************

.SUBCKT division_core GND div div1 final input output rst_n supplyVDD
*.PININFO div:I div1:I input:I rst_n:I output:O GND:B final:B supplyVDD:B
XI13 out8 net013 rst_n supplyVDD GND / divideby2
XI7 final net18 rst_n supplyVDD GND / divideby2
XI8 out2 net17 rst_n supplyVDD GND / divideby2
XI9 out4 net16 rst_n supplyVDD GND / divideby2
MC0 GND supplyVDD GND GND N_12_HSL130E W=4u L=2.1u M=5.0
XI19 div1 div1_ supplyVDD GND / INV2
XI12 net013 out16 supplyVDD GND / INV2
XI94 div div_ supplyVDD GND / INV2
XI4 input final supplyVDD GND / INV2
XI3 net18 out2 supplyVDD GND / INV2
XI18 net16 out8 supplyVDD GND / INV2
XI2 net17 out4 supplyVDD GND / INV2
XI21 div1_ div1 output net011 supplyVDD GND / tg
XI16 div1 div1_ net010 output supplyVDD GND / tg
XI20 div_ div net011 out2 supplyVDD GND / tg
XI14 div div_ out16 net010 supplyVDD GND / tg
XI92 div div_ out4 net011 supplyVDD GND / tg
XI1 div_ div net010 out8 supplyVDD GND / tg
.ENDS

************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    tg_brito
* View Name:    schematic
************************************************************************

.SUBCKT tg_brito A EN EP Y GND supplyVDD
*.PININFO EN:I EP:I A:B GND:B Y:B supplyVDD:B
MM0 A EN Y GND N_12_HSL130E W=500n L=120.0n M=1.0
MM1 A EP Y supplyVDD P_12_HSL130E W=1.5u L=120.0n M=1.0
.ENDS

************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    INV0
* View Name:    schematic
************************************************************************

.SUBCKT INV0 in out vdd vss
*.PININFO in:B out:B vdd:B vss:B
MPM1 out in vdd vdd P_12_HSL130E W=720.0n L=240.0n M=1.0
MNM0 out in vss vss N_12_HSL130E W=160.0n L=240.0n M=1.0
.ENDS

************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    INV_OSC
* View Name:    schematic
************************************************************************

.SUBCKT INV_OSC in out vdd vss
*.PININFO in:B out:B vdd:B vss:B
MPM1 out in vdd vdd P_12_HSL130E W=1.62u L=265.00n M=2.0
MNM0 out in vss vss N_12_HSL130E W=360.0n L=265.00n M=2.0
.ENDS

************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    ringOsc_HS
* View Name:    schematic
************************************************************************

.SUBCKT ringOsc_HS f_sel out start_n vctrl vdd vss
*.PININFO f_sel:I out:I vctrl:I vdd:I vss:I start_n:B
XI62 net0161 f_sel_ f_sel net0165 vss vdd / tg_brito
XI60 net0134 f_sel f_sel_ net0165 vss vdd / tg_brito
XI61 net0134 f_sel_ f_sel net0183 vss vdd / tg_brito
XI59 start_n start vdd vss / INV0
MPM37 net0134 vctrl net0134 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM36 net0130 vctrl net0130 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM35 net0128 vctrl net0128 vdd P_12_HSL130E W=2u L=4u M=2.0
MPM34 net0161 vctrl net0161 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM33 net0159 vctrl net0159 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM32 net0157 vctrl net0157 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM31 net0155 vctrl net0155 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM30 net0153 vctrl net0153 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM29 net0151 vctrl net0151 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM28 net0149 vctrl net0149 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM27 net0147 vctrl net0147 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM26 net0145 vctrl net0145 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM25 net0143 vctrl net0143 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM24 net0141 vctrl net0141 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM23 net0139 vctrl net0139 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM22 net0137 vctrl net0137 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM21 net0135 vctrl net0135 vdd P_12_HSL130E W=4u L=4u M=1.0
MPM20 out start vdd vdd P_12_HSL130E W=1.6u L=120.0n M=12.0
MNM21 net0165 start_n vss vss N_12_HSL130E W=1.44u L=120.0n M=10.0
XI68 net0128 net0130 vdd vss / INV_OSC
XI69 net0130 net0134 vdd vss / INV_OSC
XI67 net0165 net0128 vdd vss / INV_OSC
XI82 net0157 net0159 vdd vss / INV_OSC
XI83 net0159 net0161 vdd vss / INV_OSC
XI81 net0155 net0157 vdd vss / INV_OSC
XI80 net0153 net0155 vdd vss / INV_OSC
XI78 net0149 net0151 vdd vss / INV_OSC
XI77 net0147 net0149 vdd vss / INV_OSC
XI79 net0151 net0153 vdd vss / INV_OSC
XI76 net0145 net0147 vdd vss / INV_OSC
XI70 net0183 net0135 vdd vss / INV_OSC
XI71 net0135 net0137 vdd vss / INV_OSC
XI72 net0137 net0139 vdd vss / INV_OSC
XI73 net0139 net0141 vdd vss / INV_OSC
XI74 net0141 net0143 vdd vss / INV_OSC
XI75 net0143 net0145 vdd vss / INV_OSC
XI63 f_sel f_sel_ vdd vss / INV2
XI58 net0165 out vdd vss / INV2
.ENDS

************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    INV1
* View Name:    schematic
************************************************************************

.SUBCKT INV1 in out vdd vss
*.PININFO in:B out:B vdd:B vss:B
MPM1 out in vdd vdd P_12_HSL130E W=1.44e-6 L=240.0n M=1.0
MNM0 out in vss vss N_12_HSL130E W=320e-9 L=240.0n M=1.0
.ENDS

************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    FFDR
* View Name:    schematic
************************************************************************

.SUBCKT FFDR clk d q q_ r_ vdd vss
*.PININFO clk:B d:B q:B q_:B r_:B vdd:B vss:B
MPM6 net017 r_ vdd vdd P_12_HSL130E W=720.0n L=120.0n M=1.0
MPM5 net017 net014 vdd vdd P_12_HSL130E W=720.0n L=120.0n M=1.0
MPM4 net014 clk vdd vdd P_12_HSL130E W=720.0n L=120.0n M=1.0
MPM2 net30 d vdd vdd P_12_HSL130E W=1.44e-6 L=120.0n M=1.0
MPM0 net15 clk net30 vdd P_12_HSL130E W=1.44e-6 L=120.0n M=1.0
MNM3 net32 net014 vss vss N_12_HSL130E W=320e-9 L=120.0n M=1.0
MNM2 net017 clk net32 vss N_12_HSL130E W=320e-9 L=120.0n M=1.0
MNM0 net33 clk vss vss N_12_HSL130E W=320e-9 L=120.0n M=1.0
MNM1 net014 net15 net33 vss N_12_HSL130E W=320e-9 L=120.0n M=1.0
MPM1 net15 d vss vss N_12_HSL130E W=160.0n L=120.0n M=1.0
XI2 q q_ vdd vss / INV1
XI1 net017 q vdd vss / INV1
.ENDS

************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    pulse_control
* View Name:    schematic
************************************************************************

.SUBCKT pulse_control control_pulse start_pulse stop_pulse vdd vss
*.PININFO start_pulse:I stop_pulse:I control_pulse:O vdd:B vss:B
XI0 stop_pulse vdd q q_n start_pulse vdd vss / FFDR
MNM0 start_pulse q_n control_pulse vss N_12_HSL130E W=160.0n L=120.0n M=1.0
MNM1 control_pulse q stop_pulse vss N_12_HSL130E W=800n L=120.0n M=1.0
MPM1 control_pulse q_n stop_pulse vdd P_12_HSL130E W=1.5u L=120.0n M=1.0
MPM0 start_pulse q control_pulse vdd P_12_HSL130E W=280.0n L=120.0n M=1.0
.ENDS

************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    buffer_jrf
* View Name:    schematic
************************************************************************

.SUBCKT buffer_jrf GND Input Output supplyVDD
*.PININFO Input:I Output:O GND:B supplyVDD:B
MNM16 Output net22 GND GND N_12_HSL130E W=1.44e-6 L=120.0n M=9.0
MNM1 sec prim GND GND N_12_HSL130E W=480.0n L=120.0n M=1.0
MNM0 prim Input GND GND N_12_HSL130E W=320e-9 L=120.0n M=1.0
MNM12 tri sec GND GND N_12_HSL130E W=1.44u L=120.0n M=1.0
MNM13 net22 tri GND GND N_12_HSL130E W=1.152e-6 L=120.0n M=3.0
MPM0 sec prim supplyVDD supplyVDD P_12_HSL130E W=1.44u L=120.0n M=1.0
MPM3 prim Input supplyVDD supplyVDD P_12_HSL130E W=960e-9 L=120.0n M=1.0
MPM1 tri sec supplyVDD supplyVDD P_12_HSL130E W=3.456e-6 L=120.0n M=1.0
MPM6 Output net22 supplyVDD supplyVDD P_12_HSL130E W=4.32e-6 L=120.0n M=9.0
MPM2 net22 tri supplyVDD supplyVDD P_12_HSL130E W=4.32u L=120.0n M=3.0
.ENDS

************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    top
* View Name:    schematic
************************************************************************

.SUBCKT top GND div0 div1 f_sel final supplyVDD supplyVDD2 vtune wide_pulse
*.PININFO div0:I div1:I f_sel:I GND:B final:B supplyVDD:B supplyVDD2:B vtune:B 
*.PININFO wide_pulse:B
XI7 control_pulse control_inv supplyVDD GND / INV2
XI3 GND div0 div1 net21 oscillator divider_output wide_pulse supplyVDD / 
+ division_core
XI12 f_sel oscillator control_inv vtune supplyVDD GND / ringOsc_HS
XI11 control_pulse wide_pulse divider_output supplyVDD GND / pulse_control
XI68 GND net21 final supplyVDD / buffer_jrf
MC0 GND supplyVDD GND supplyVDD P_12_HSL130E W=10u L=10u M=20.0
MC1 GND supplyVDD GND GND N_12_HSL130E W=4u L=2.1u M=22.0
.ENDS

************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    esd
* View Name:    schematic
************************************************************************

.SUBCKT esd GND PAD VDD
*.PININFO GND:B PAD:B VDD:B
MNM0 PAD GND GND GND N_HG_33_L130E W=3u L=440.0n M=40.0
MPM0 PAD VDD VDD VDD P_HG_33_L130E W=3u L=440.0n M=40.0
.ENDS

************************************************************************
* Library Name: UNSEEN_UWB_1V2
* Cell Name:    top_w_pads
* View Name:    schematic
************************************************************************

.SUBCKT top_w_pads GND div0 div1 f_sel output supplyVDD vtune wide_pulse
*.PININFO div0:I div1:I output:I GND:B f_sel:B supplyVDD:B vtune:B wide_pulse:B
XR4 f_sel f_sel_in supplyVDD GND / RNNPO_RF R=309.6019 w=2u l=5u d=500n m=1
XR3 vtune vtune_in supplyVDD GND / RNNPO_RF R=309.6019 w=2u l=5u d=500n m=1
XR2 wide_pulse_in wide_pulse supplyVDD GND / RNNPO_RF R=309.6019 w=2u l=5u 
+ d=500n m=1
XR1 div1_in div1 supplyVDD GND / RNNPO_RF R=309.6019 w=2u l=5u d=500n m=1
XR0 div0_in div0 supplyVDD GND / RNNPO_RF R=309.6019 w=2u l=5u d=500n m=1
XC10 GND GND / PAD_RF w=74.2u l=80.8u index=1 tm=8 C=263.7958f
XC8 div1 GND / PAD_RF w=74.2u l=80.8u index=1 tm=8 C=263.7958f
XC7 supplyVDD GND / PAD_RF w=74.2u l=80.8u index=1 tm=8 C=263.7958f
XC6 GND GND / PAD_RF w=74.2u l=80.8u index=1 tm=8 C=263.7958f
XC5 div0 GND / PAD_RF w=74.2u l=80.8u index=1 tm=8 C=263.7958f
XC2 wide_pulse GND / PAD_RF w=74.2u l=80.8u index=1 tm=8 C=263.7958f
XC4 vtune GND / PAD_RF w=74.2u l=80.8u index=1 tm=8 C=263.7958f
XC1 output GND / PAD_RF w=74.2u l=80.8u index=4 tm=8 C=89.2058f
XC9 f_sel GND / PAD_RF w=74.2u l=80.8u index=1 tm=8 C=263.7958f
XC3 supplyVDD GND / PAD_RF w=74.2u l=80.8u index=1 tm=8 C=263.7958f
XI12 GND div0_in div1_in f_sel_in output supplyVDD net31 vtune_in 
+ wide_pulse_in / top
XI34 GND vtune supplyVDD / esd
XI33 GND f_sel supplyVDD / esd
XI32 GND wide_pulse supplyVDD / esd
XI30 GND div0 supplyVDD / esd
XI31 GND div1 supplyVDD / esd
.ENDS

